local assets = require("assets")
local X = require("declaration")
local entity = require("entity")
local printer = require("printer")

-- keeps a pointer to every entity with a stat
local ENTITES = {}

-- Init and declaration validity check
local function init ()
	for _, s in ipairs(X.stats) do
		if s.min and s.max then
			assert(s.min < s.max, "Maximum and miminum of stat is not sensible: " .. s.key)
		end
	end
end

-- finds and returns a stat on an entity
local function find ( e, key )
	-- TODO: could this use entity.find?
	for _, s in ipairs(e.stats) do
		if s.key == key then
			return s
		end
	end
	return nil
end

-- remove a stat if it exists
local function remove (e, key)
	for i, s in ipairs(e.stats) do
		if s.key == key then
			table.remove(e.stats, i)
			return true
		end
	end
	return false
end

-- just creates a fresh statistic
local function create (e, key, value)
	assert(tonumber(value), "Creating stats requires a number")
	local s = entity.copy( entity.find(X.stats, key) )
	s.value = value
	-- prevent doubles
	remove(e, key)
	table.insert(e.stats, s)
	table.insert(ENTITES, e)
	print("Statted", e.key, key)
	return true
end

local function copy (from, to, key)
	printer.trace("copying stat", from.key, to.key, key)
	local s = entity.copy( find(from, key) )
	remove(to, key)
	table.insert(to.stats, s)
	table.insert(ENTITES, to)
	return true
end

-- tests if a change can be afforded and also returns the new value
local function canAfford (e, statKey, change)
	local stat = find (e, statKey)
	if not stat then
		return false
	end
	local newValue = stat.value + change
	if stat.max and newValue > stat.max then
		if entity.hasType(stat, "ignore_overflow") then
			return true, stat.max
		else
			return false
		end
	elseif stat.min and newValue < stat.min then
		if entity.hasType( stat, "ignore_underflow") then
			return true, stat.min
		else
			return false
		end
	end
	return true, newValue
end

-- commit a change of value
local function change (e, statKey, change)
	local ok, val = canAfford(e, statKey, change)
	if ok then
		local stat = find (e, statKey)
		stat.value = val
	end
	printer.trace("Changed stat?", ok, e.key, statKey, change, val)
	return ok
end

-- just set a value, does not care about min/max (right now)
local function replace (e, statKey, newValue)
	local stat = find(e, statKey)
	stat.value = newValue
	return true
end

-- replaces a given block of stats on an entity with its full set
local function fill (e)
	local map = e.stats
	e.stats = {}
	if map then
		for k, v in pairs(map) do
			create(e, k, v)
		end
	end
	--printer.trace("filled stats!", e.key, e.stats)
end

-- draws a bunch of stats
local function draw ( stats, place )
	love.graphics.push("all")
	for i, a in ipairs(stats) do
		local c = a.color or { 1, 1, 1 }
		love.graphics.setColor(unpack(c))
		love.graphics.printf(a.key .. ":" .. tostring(a.value), place.x, place.y + 35*i, place.w)
	end
	love.graphics.pop()
end

return {
	create = create,
	remove = remove,
	copy = copy,
	change = change,
	canAfford = canAfford,
	init = init,
	fill = fill,
	find = find,
	draw = draw,
}