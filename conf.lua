-- love needs this
local X = require("declaration")

function love.conf(t)
	t.console = true
	t.window.width = 1024
	t.window.height = 576
	t.modules.joystick = false
	t.modules.physics = false
end