local X = require("declaration")
local entity = require("entity")
local printer = require("printer")

local ACTIVE_PHASE = nil
local ACTIVE_ACTIONS = {}

local function activatePhase ( key )
	ACTIVE_PHASE = entity.find(X.phases, key)
	ACTIVE_ACTIONS = {}
	for _, key in ipairs(ACTIVE_PHASE.actions) do
		local act = entity.find(X.actions, key)
		table.insert(ACTIVE_ACTIONS, act)
	end
	print("Activated phase", key.key or key)
	
	if entity.hasType(ACTIVE_PHASE, "automatic") then
		return ACTIVE_ACTIONS
	end
end

local function nextPhase ()
	if ACTIVE_PHASE.successor then
		activatePhase( ACTIVE_PHASE.successor )
	end
	return activatePhase( entity.findSuccessor(X.phases, ACTIVE_PHASE.key))
end

-- returns the currently active actions
local function activeActions ( aType )
	if entity.hasType(ACTIVE_PHASE, aType) then
		return ACTIVE_ACTIONS
	else
		return {}
	end
end

local function get()
	return ACTIVE_PHASE
end

return {
	activatePhase = activatePhase,
	nextPhase = nextPhase,
	activeActions = activeActions,
	get = get,
}
