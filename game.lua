local phase = require("phase")
local eval = require("eval")
local printer = require("printer")
local X = require("declaration")
local entity = require("entity")

local MODE = "player_input"

-- attempts to runs a single action
local function runHelper (action, interaction)
	eval.pushState()
	local status, success = xpcall(eval.attempt, debug.traceback, action, interaction)
	if not status then
		printer.trace("Eval crashed", success, action.key, interaction)
	end
	
	-- if we have events or selectors, deal with them
	local state = eval.state()
	for _, e in ipairs(state.events) do
		-- run q/c of reactions, but this might also produce reactions, so we need to recursive call here
		-- how do we deal with references from the old state?
		local reactor = e.reactor
		local reaction = e.reaction
		print ("reacting to events")
		-- pass reactor as the interaction, this makes it visible in the params
		runHelper(reaction, { reactor = reactor })
	end
	
	eval.popState()
	
	return success
end


-- for every valid action in phase check their query, if that succeeds, execute it
local function runAll ( actions, interaction )
	-- should probably deal with stack overflow here?
	local success = true
	for _, action in ipairs(actions) do
		success = success and runHelper (action, interaction)
	end
	-- if all actions succeeded, we might need to end a selector or change phase?
end

-- button is either 1 (left) or 2 (right), defaulting to left
local function click (x, y, button)
	local clicktype = button == 1 and "lmb" or "rmb"
	local actions = phase.activeActions( MODE )
	runAll( actions, {[clicktype] = { x, y }} )
end

-- drag always with LMB
local function drag (x1,y1, x2,y2)
	local actions = phase.activeActions( MODE )
	runAll( actions, {drag = { x1, y1, x2, y2 } })
end

local function keyClicked ( key )
	local actions = phase.activeActions( MODE )
	runAll( actions, {key = key} )
end

-- pass UI info to this class, which then calls game.execute on it.
local function init()
	local actions = phase.activatePhase(X.phases[1])
	while actions and #actions > 0 do
		runAll( actions, nil)
		actions = phase.nextPhase()
	end
end

return {
	mouseClicked = click,
	mouseDragged = drag,
	keyClicked = keyClicked,
	init = init,
}