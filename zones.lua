local X = require("declaration")
local entity = require("entity")
local printer = require("printer")
local assets = require("assets")
local card = require("card")
local tokens = require("tokens")
local stats = require("stats")

-- all currently active zones
local ZONES = {}

local DIRTY = false

-- adds a zone into the active set (and into ZONES)
local function activatezone (s)
	-- while not all zones can have all content, the code is much worse to write without the empty arrays
	s.squares = {}
	s.cards = {}
	s.stats = {}
	
	printer.trace("Activated zone", s.key)
	table.insert(ZONES, s)
end

local function makeSquare (name)
	for _, q in ipairs(X.squares) do
		if q.key == name then
			local c = entity.copy(q)
			c.place, c.tokens, c.stats = {}, {}, {}
			return c
		end
	end
	assert(false, "bad name given " .. name)
end

local function init()
	for _, zone in ipairs(X.zones) do
		local s = entity.copy(zone)
		activatezone(s)
	end
end

-- adds the actual coordinates into a raw place from a declaration
local function basicPlace ( p, margins )
	local w, h = love.graphics.getDimensions( )
	p.x = p[1] * w + margins
	p.y = p[2] * h + margins
	p.w = (p[3] - p[1])*w - 2*margins
	p.h = (p[4] - p[2])*h - 2*margins
end

local function resize (force)
	if not (DIRTY or force) then
		return
	end
	local w, h = love.graphics.getDimensions( )
	local margins = X.lookAndFeel.zones.margins or 4
		
	local function resizeSquarezone (s)
		local p = s.place
		for _, q in ipairs(s.squares) do
			local pq = q.place
			if pq.gridX then
				pq.x = p.x + (pq.gridX-1) * (p.w-margins) / s.size[1] + margins
				pq.y = p.y + (pq.gridY-1) * (p.h-margins) / s.size[2] + margins
				local size = math.min(p.w / s.size[1] - margins, p.h / s.size[2] - margins)
				pq.w = size
				pq.h = size
				
				local tSize = (size - 3*margins) *0.5
				for i, token in ipairs(q.tokens) do
					local t = token.place
					if i == 1 then
						t.x = pq.x + margins
						t.y = pq.y + margins
					elseif i == 2 then
						t.x = pq.x + margins*2 + tSize
						t.y = pq.y + margins
					elseif i == 3 then
						t.x = pq.x + margins
						t.y = pq.y + margins*2 + tSize
					elseif i == 4 then
						t.x = pq.x + margins*2 + tSize
						t.y = pq.y + margins*2 + tSize
					end
					t.w = tSize
					t.h = tSize
				end
			end
		end
	end
	
	local function resizeCardzone (s)
		local p = s.place
		local lastX = p.x + margins
		local cardH = p.h - margins*2
		local cardW = cardH / 1.61
		for _, c in ipairs(s.cards) do
			-- TODO: How do I want to show cards better?
			local pq = c.place
			pq.x = lastX + margins
			pq.y = p.y + margins
			pq.h = cardH
			pq.w = cardW
			lastX = pq.x + cardW
		end
	end
	
	for _, s in ipairs(ZONES) do
		if entity.hasAnyType(s, {"board", "stats", "hand"}) then
			basicPlace(s.place, margins)
		end
		
		if entity.hasType(s, "board") then
			resizeSquarezone(s)
		elseif entity.hasType(s, "hand") then
			basicPlace(s.place, margins)
			resizeCardzone(s)
		end
	end
	
	DIRTY = false
end

local function drawzones()
	love.graphics.push("all")
	
	local function drawContainerzone (s)
		love.graphics.push("all")
		local p = s.place
		--printer.trace(s)
		local c = s.color or { 1, 1, 1 }
		love.graphics.setColor(unpack(c))
		love.graphics.rectangle("line", p.x, p.y, p.w, p.h)
		love.graphics.pop()
	end
	
	local function drawSquarezone (s)
		local p = s.place
		for _, q in ipairs(s.squares) do
			love.graphics.push("all")
			local p = q.place
			local c = q.color or { 0, 0, 0 }
			love.graphics.setColor(unpack(c))
			love.graphics.rectangle("fill", p.x, p.y, p.w, p.h)
			love.graphics.setColor({ 1, 1, 1})
			if q.asset then
				assets.draw(q.asset, p.x, p.y, p.w, p.h, "force")
			end
			love.graphics.pop()
			for _, t in ipairs(q.tokens) do
				tokens.drawOneSmall(t)
			end
		end
	end
	
	local function drawStatzone (s)
		stats.draw(s.stats, s.place)
	end
	
	for _, s in ipairs(ZONES) do
		if entity.hasAnyType(s, {"board", "hand", "stats"}) then
			drawContainerzone(s)
		end
		
		if entity.hasType(s, "board") then
			drawSquarezone(s)
		elseif entity.hasType(s, "stats") then
			drawStatzone(s)
		end
		-- cards are drawn by the own module
	end
	
	love.graphics.pop()
end

local function isInside (place, x, y)
	assert(place, "need place")
	assert(x and y, "need position")
	return x >= place.x and x <= place.x + place.w 
			and y >= place.y and y <= place.y + place.h
end

-- returns whatever things are on a specific coordinate
local function locate (x, y, kind)
	if not x or not y then
		print("Locate needs x and y", x, y)
		return {}
	end
	local results = {}
	for _, s in ipairs(ZONES) do
		-- excludes zones that do not have a place, such as decks
		local p = s.place
		if p and isInside(p, x, y) then
			table.insert(results, s)
			for _, q in ipairs(s.squares) do
				if q.place and isInside(q.place, x, y) then
					table.insert(results, q)
					
					for _, t in ipairs(q.tokens) do
						if t.place and isInside(t.place, x, y) then
							table.insert(results, t)
						end
					end
				end
			end
			for _, c in ipairs(s.cards) do
				if c.place and isInside(c.place, x, y) then
					table.insert(results, c)
				end
			end
		end
	end
	
	-- filter for kind if needed
	if kind then
		local final = {}
		for _, x in ipairs(results) do
			if x.kind == kind then
				table.insert(final, x)
			end
		end
		results = final
	end
	return results
end

local function find ( key )
	return entity.find(ZONES, key, "zone")
end

local function pushCard (zone, card)
	assert(card, "did not find card " .. tostring(card))
	
	-- remove it card from any other other zones
	for _, s in ipairs(ZONES) do
		for i, c in ipairs(s.cards) do
			if c == card then
				printer.trace("pushCard: Removed card from zone", s.key)
				table.remove(s.cards, i)
				break
			end
		end
	end
	
	local s = entity.find(ZONES, zone, "zone")
	--printer.trace("finding in zones", ZONES, zone, s)
	if not s then
		printer.trace("pushCard: Could not find matching zone", zone)
		return false
	end
	
	if s.place then
		card.place = {}
	else
		card.place = nil
	end	
	print("Pushed card", card.key)
	table.insert(s.cards, card)
	DIRTY = true
	resize()
	return true
end

local function topCard (zoneKey)
	local s = entity.find(ZONES, zoneKey, "zone")
	return s and s.cards[#s.cards]
end


-- adds a token to a square
local function pushToken (square, token)
	if #square.tokens >= 4 then
		print("Too many tokens on this square")
		return false
	end
	printer.trace("push token", square.key, token.key)
	token.place = {}
	assert(entity.hasType(square, "inhabitable"), "Square seems not inhabitable?")
	table.insert(square.tokens, token)
	printer.trace("inserted token!", token.key)
	DIRTY = true
	resize()
	return true
end

local function countTokens (square)
	return #square.tokens
end

local function pushSquare (zone, square, pos)
	-- TODO: Think about how to do these distinctions in a convenient manner?
	assert(zone)
	assert(square)
	assert(entity.hasType(zone, "board"))
	if entity.hasType(zone, "grid") then
		local place = zone.place
		local gridX, gridY
		if pos.kind == "gridXY" then
			gridX = pos.x
			gridY = pos.y
		elseif pos.kind == "screenXY" then
			gridX = math.floor( (pos.x - place.x) / (place.w / zone.size[1])) +1
			gridY = math.floor( (pos.y - place.y) / (place.h / zone.size[2])) +1
		end
		
		if gridX > zone.size[1] or gridX < 1 or gridY < 1 or gridY > zone.size[2] then
			print("cannot add square, out of bounds")
			return false
		end
		
		-- check for duplicates
		for _, q in ipairs(zone.squares) do
			if q.place.gridX == gridX and q.place.gridY == gridY then
				print("cannot add square, already there")
				return false
			end
		end
		
		square.place.gridX = gridX
		square.place.gridY = gridY
		printer.trace("Added square to zone", square.key, zone.key)
		table.insert(zone.squares, square)
		DIRTY = true
		resize()
		return true
	end
	return false
end

local function removeSquare (square)
	assert(square)
	--printer.trace("square to be removed", square)
	for _, zone in ipairs(ZONES) do
		for i, q in ipairs(zone.squares) do
			--printer.trace("checking square", q)
			if q == square then
				table.remove(zone.squares, i)
				print("removed square", square.key)
				DIRTY = true
				resize()
				return true
			end
		end
	end
	print("failed to remove square")
	return false
end

-- 
local function connectSquares ( from, to, name )
	assert(from)
	assert(to)
	
end

-- returns every zone/square/token which matches all given filters
local function filtered ( filters )
	print("filtered with N filters:", #filters)
	local results = {}
	
	local function test ( e )
		for _, f in ipairs(filters) do
			if not f(e) then
				return
			end
		end
		table.insert(results, e)
	end
	
	-- iterate over everything
	for _, z in ipairs(ZONES) do
		test(z)
		for _, s in ipairs(z.squares) do
			test(s, z)
			for _, t in ipairs(s.tokens) do
				test(t, s, z)
			end
		end
		for _, c in ipairs(z.cards) do
			test(c, z)
		end
	end
	return results
end

local function positionFilter ( position )
	return position and tonumber(position.x) and tonumber(position.y) and function (e)
		return e.place and isInside(e.place, position.x, position.y)
	end
end

-- a filter that is true for everything inside a square or zone of a given key
local function insideFilter ( key )
	return key and function (t, q, s)
		return q.key == key or s.key == key
	end
end

-- filter that finds entities that have a reaction defined
local function reactorFilter ()
	return function (e)
		return e.reactions and #e.reactions > 0
	end
end

-- return all entities that could react to an action
local function reactors ()
	local filters = { reactorFilter() }
	return filtered( filters )
end

return {
	draw = drawzones,
	resize = resize,
	init = init,
	pushCard = pushCard,
	topCard = topCard,
	pushToken = pushToken,
	countTokens = countTokens,
	pushSquare = pushSquare,
	makeSquare = makeSquare,
	removeSquare = removeSquare,
	connectSquares = connectSquares,
	locate = locate,
	find = find,
	filtered = filtered,
	positionFilter = positionFilter,
	basicPlace = basicPlace,
	reactors = reactors,
}