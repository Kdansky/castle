local X = require("declaration")
local entity = require("entity")
local printer = require("printer")
local assets = require("assets")
local stats = require("stats")

-- Init and declaration validity check
local function init ()
end

-- just creates a fresh token
local function create (key)
	local blueprint = entity.find(X.tokens, key)
	assert(blueprint, "Creating token failed" .. tostring(key))
	
	local token = entity.copy(blueprint)
	stats.fill(token)
	return token
end

local function drawOneSmall (token)
	love.graphics.push("all")
	-- frame
	local p = token.place
	love.graphics.setColor(0.137, 0.7, 0.05)
	love.graphics.rectangle("line", p.x, p.y, p.w, p.h, 3, 3)
	love.graphics.setColor(1, 1, 1)
	assets.draw(token.asset, p.x+1, p.y+1, p.w-2, p.h-2)
	love.graphics.pop()
end

return {
	init = init,
	create = create,
	drawOneSmall = drawOneSmall
}