local X = require("declaration")
local entity = require("entity")
local zones = require("zones")
local card = require("card")
local stats = require("stats")
local printer = require("printer")
local tokens = require("tokens")
local phase = require("phase")

local print = printer.trace

--[[
	All functions can return the boolean <false>, which will immediately end the
	current evaluation. False is therefore not a valid result of any check,
	nor must it be checked against, as no functions will ever receive it as
	their parameters.
	
	I would have used nil, but that behaves really badly with array iteration
	and map entries. A fixed magic string would also work, but it's just more code.
]]

------------------------ EVAL functions -----------------------------------------
local eval = {}
local eval2 = {}

-- a map of current evaluation state
local STATE = {}

-- in case of events, this contains the state that produced the event
local OLD_STATE = {}

-- forward declare
local evaluate

---------------------------- STATE handling --------------------------------------
local PREV_STATES = {}
local function pushState()
	table.insert(PREV_STATES, OLD_STATE)
	OLD_STATE = STATE
	STATE = {
		events = {}
	}
end

local function popState()
	STATE = OLD_STATE
	OLD_STATE = PREV_STATES[#PREV_STATES]
	table.remove(PREV_STATES, #PREV_STATES)
end


----------------------------- Events -------------------------------

function eval.reactor ()
	return STATE.reactor or false
end

-- unused?
function eval.reaction ()
	return STATE.reaction or false
end

-- unused?
function eval.trigger ()
	return STATE.trigger or false
end

local function possibleEvents ( reactors, functionName, params)
	local events = {}
	for _, e in ipairs(reactors or {}) do
		for _, r in ipairs(e.reactions or {}) do
			if r.trigger[1] == functionName then
				
				print("testing reaction", r, params)
				-- this should check every param whether it is a match against the given equivalent thing
				-- could be array or map, does not really matter
				for k, val in pairs(params) do
					local wanted = r.trigger[k]
					
					if wanted == val then
						table.insert(events, {reactor = e, reaction = r})
						break
					end
				end
			end
		end
	end
	return events
end

-------------------------------Meta---------------------------------


-- puts parameters into the state if given
function eval.set ( key, value )
	STATE.params = STATE.params or {}
	STATE.params[key] = value
	return true
end

function eval.get ( key )
	return STATE.params and STATE.params[key] or false
end

function eval.event_reference ( key )
	return OLD_STATE[key] or false
end

-- Runs multiple commands, discarding all return values
function eval.multi (...)
	return true
end

-- Runs multiple commands, returning the AND of all return values
function eval.all (...)
	local result = true
	for _, v in ipairs({...}) do
		result = result and v
	end
	return result
end

---------------------------- Zones ---------------------------------

-- produces an returns an entity (a zone, in this kind)
-- giving both key and position will make sure they match. Giving only one will search by it.
function eval.zone_at (key, position, player)
	local z1, z2
	if key ~= "" then
		z1 = zones.find( key, "zone" )
	end
	if position and position.kind == "screenXY" then
		z2 = zones.locate( position.x, position.y, "zone" )[1]
	end
	if z1 and z2 then
		if z1 ~= z2 then
			return false
		end
	end
	z1 = z1 or z2
	STATE["referenced_zone"] = z1
	return z1 or false
end

---------------------------------Squares---------------------------------
-- creates a square from key + anEntity structure
function eval.create_square ( squareKey, anEntity, position )
	assert(anEntity.kind == "zone")
	local q = zones.makeSquare(squareKey)
	STATE["created_square"] = q
	return zones.pushSquare( anEntity, q, position)
end

-- removes a square at the given position
function eval.remove_square ( position )
	if not position.kind == "screenXY" then
		assert(false, "TODO: write other zone_at kind")
	end
	local list = zones.locate(position.x, position.y, "square")
	for _, entry in ipairs(list) do
		return zones.removeSquare( entry )
	end
	return false
end

-- finds the square that matches the position and optionally key
function eval.square_at (key, position, player)
	if not position.kind == "screenXY" then
		print("TODO: write other square_at kind")
	end
	local list = zones.locate(position.x, position.y, "square")
	for _, entry in ipairs(list) do
		if key == "any" or entry.key == key then
			STATE["referenced_square"] = entry
			return entry
		end
	end
	return false
end

-- checks if a square is in a given zone
function eval.square_in ( key, aSquare )
	local filters = {
		zones.insideFilter ( key ),
		entity.kindFilter ("zone")
	}
	local zs = zones.filtered(filters)
	for _, q in ipairs(zs.squares) do
		if aSquare == q then
			return true
		end
	end
	return false
end

-- finds the next square in a given cardinal direction (N, S, W, E)
function eval.square_dir ( aSquare, direction )
	assert(0, "todo")
end

-----------------------------cards---------------------------------

-- creates a new card in a given entity (must be a zone)
function eval.create_card ( cardKey, anEntity )
	local c = card.create( cardKey)
	STATE["created_card"] = c
	return zones.pushCard( anEntity, c)
end

function eval.push_card ( aCard, aZone )
	assert(aZone.kind == "zone")
	STATE["referenced_card"] = aCard
	return zones.pushCard ( aZone, aCard )
end

-- returns the top card of a deck (or hand)
function eval.top_card ( zoneKey )
	local c = zones.topCard( zoneKey )
	STATE["referenced_card"] = c
	return c or false
end

function eval.card_count ( zoneKey )
	local z = zones.find(zoneKey)
	return #z.cards
end

-- huh, same thing?
function eval.card_at ( key )
	assert(false, "MISSING FUNCTIONALITY")
	return false
end

-- returns the top-most card at a mouse-pos
function eval.any_card_at ( position )
	if not position then
		return false
	end
	local cs = zones.locate ( position.x, position.y, "card")
	--printer.trace("any card at?", position, cs)
	STATE["referenced_card"] = cs[#cs]
	return cs[#cs] or false
end

function eval.delete_card (  )
	assert(false)
end

-- fills a deck with cards and then shuffles it
-- technically could be done through a combination of more basic actions?
function eval.fill_deck ( aDeck, ... )
	local key = nil
	for i, v in ipairs({...}) do
		if type(v) == "string" then
			key = v
			eval.create_card (key, aDeck)
		elseif type(v) == "number" then
			for j=2, v do
				eval.create_card (key, aDeck)
			end
		end
	end
	entity.shuffle(aDeck.cards)
	return true
end

-----------------------------tokens------------------------------------

function eval.create_token ( key, aSquare )
	assert(type(key) == "string")
	assert(aSquare.kind == "square")
	local t = tokens.create( key )
	STATE["created_token"] = t
	return zones.pushToken( aSquare , t)
end

-- returns true if a token can be put on a square (could be better)
function eval.token_square ( aSquare )
	return entity.hasType( aSquare, "inhabitable") and
			zones.countTokens( aSquare) < 4
end

function eval.token_at ( spaceKey, position, player )
	local token = zones.locate ( position.x, position.y, "token")[1]
	STATE["referenced_token"] = token
	return token or false
end

---------------------------entities------------------------------------

function eval.entity_type ( anEntity, aType )
	return entity.hasType( anEntity, aType )
end

function eval.copy_asset (from, to)
	to.asset = from.asset
	return true
end

-- runs the query of an entity to see if the entity can be played
function eval.entity_query ( anEntity )
	STATE["self"] = anEntity
	local res = true
	if anEntity.query then
		res = evaluate( anEntity.query )
	end
	for _, a in ipairs (anEntity.actions or {}) do
		res = res and eval.action_query(a)
	end
	return res
end

-- activates the command of an entity
function eval.entity_command (anEntity )
	STATE["self"] = anEntity
	local res = true 
	if anEntity.command then
		res = evaluate( anEntity.command )
	end
	for _, a in ipairs (anEntity.actions or {}) do
		res = res and eval.action_command(a)
	end
	return res	
end

function eval.entities_not_equal ( a, b )
	assert(type(a) == "table")
	assert(type(b) == "table")
	return a ~= b
end

function eval.entities_equal ( ... )
	local t = {...}
	if not #t > 1 then
		return false
	end
	local prev = t[1]
	for _, v in ipairs(t) do
		if v ~= prev then
			return false
		end
		prev = v
	end	
	return true
end	

-------------------------- references ---------------------------------
-- could be done with a "state" function, but that complicates the declaration...
function eval.referenced_card ()	return STATE["referenced_card"] or false	end
function eval.referenced_square()	return STATE["referenced_square"] or false	end
function eval.referenced_token()	return STATE["referenced_token"] or false	end
function eval.created_card ()	return STATE["created_card"] or false	end
function eval.created_square ()	return STATE["created_square"] or false	end
function eval.created_token ()	return STATE["created_token"] or false	end

function eval.referenced_multiple (num) 
	return STATE["referenced_multiple"] and 
			STATE["referenced_multiple"][num] or false
end

function eval.reactor()	return STATE["reactor"] or false	end
function eval.self ()	return STATE["self"] or false	end


function eval.counting_cards_or_squares_someow_and_comparignthem ( position )
	-- TODO: figure out an elegant way of doing this
	assert(false)
	return false
end

function eval.param ( name )
	return STATE[name] or false
end

function eval.event_param ( name ) 
	return OLD_STATE[name] or false
end

-------------------------- positions ---------------------------------
-- produces a position struct
function eval.gridpos (x, y)
	return { kind = "gridXY", x = x, y = y}
end
	
-- produces a position struct
function eval.mouse_position ()
	local x, y = love.mouse.getPosition()
	STATE["mousepos"] = { x = x, y = y }
	return { kind = "screenXY", x = x, y = y}
end

------------------------ UI interactions--------------------------------
-- produces a position struct
function eval.mouse_dragged_from ()
	local d = STATE["drag"]
	if not d or not #d == 4 then
		return false
	end
	return { kind = "screenXY", x = d[1], y = d[2] }
end

-- produces a position struct
function eval.mouse_dragged_to ()
	local d = STATE["drag"]
	if not d or not #d == 4 then
		return false
	end
	return { kind = "screenXY", x = d[3], y = d[4] }
end

-- check if we are clicking
function eval.rmb_clicked ()
	return STATE["rmb"] or false
end

-- check if we are clicking
function eval.lmb_clicked ()
	return STATE["lmb"] or false
end

function eval.key_clicked ( button )
	return STATE["key"] == button
end

-- call a different action, optionally multiple times (deprecated)
function eval.action_command ( key, count )
	assert(key, "Execute Action needs a key")
	local a = entity.find(X.actions, key)
	
	local result = true
	for i = 1, (count or 1), 1 do
		printer.trace("evaluate running on", key, count, a)
		result = result and evaluate( a.command )
	end
	return result
end

-- call a different action (deprecated)
function eval.action_query ( key )
	local a = entity.find(X.actions, key)
	return evaluate( a.query )
end

-- modern syntax?
function eval.execute ( args )
	-- part, count, action, key
	
	args.part = args.part or "all"
	args.count = args.count or 1
	if not args.action then
		args.action = entity.find(X.actions, args.key)
	else
		args.key = args.action.key
	end
	
	assert(args.action, "execute was given no action")
	assert(args.part == "all" or args.part == "query" or args.part == "command")
	
	for k, v in pairs(args.params or {}) do
		eval.set(k, v)
	end
	printer.trace("evaluate running on", args)
		
	local function run (f)
		local result = true
		for i = 1, args.count, 1 do
			result = result and evaluate( f )
		end
		return result
	end
	
	local result = true
	if args.part == "all" or args.part == "query" then
		result = args.action.query and run(args.action.query) or true
	end
	
	if args.part == "all" or args.part == "command" then
		result = result and run(args.action.command)
	end
	return result
end


-- Checks if a drag was between two entities
function eval.drag ( from_entity, to_entity )
	local d = STATE["drag"]
	if not d or not #d == 4 then
		return false
	end
	local froms = zones.locate( d[1], d[2] )
	local tos = zones.locate( d[3], d[4] )
	
	for _, from in ipairs( froms ) do
		if from == from_entity then
			for _, to in ipairs( tos ) do
				if to == to_entity then
					return true
				end
			end
		end
	end
	return false
end

---------------------------Stats---------------------------------

function eval.create_stat ( statKey, anEntity, val )
	local stat = stats.create( anEntity, statKey, val)
	STATE["created_stat"] = stat
	return stat
end

-- works, but very repetitive?
function eval.stat_can_decrease ( statKey, anEntity, value)
	return stats.canAfford( anEntity, statKey, -1*value )
end
function eval.stat_decrease (statKey, anEntity, value)
	return stats.change( anEntity, statKey, -1*value )
end
function eval.stat_increase (statKey, anEntity, value)
	return stats.change( anEntity, statKey, value )
end

function eval.stat_property ( statKey, anEntity, property )
	assert(property == "max" or property == "mind" or property == "value")
	return stats.find ( anEntity, statKey )[property] or false
end

function eval.copy_stat ( statKey, fromEntity, toEntity )
	return stats.copy (fromEntity, toEntity, statKey)
end

-- applies a change to multiple entities, returns true if at least one application was successful
function eval.multi_stat ( stat_function, statKey, someEntities, value)
	local result = false
	for _, e in ipairs(someEntities) do
		result = eval[stat_function](statKey, e, value) or result
	end
	return result
end

---------------------------Phases---------------------------------

function eval.end_phase ()
	return phase.nextPhase()
end

function eval.activate_phase (key)
	local actions = phase.activatePhase(key)
	STATE["activated_phase"] = key
	while actions and #actions > 0 do
		for _, a in ipairs(actions) do
			eval.action_command ( a )
			actions = phase.nextPhase()
		end
	end
end

function eval.phase_is (key)
	return phase.get().key == key
end

---------------------------Filler---------------------------------
-- always returns true but noisly ()
function eval.test ( value )
	printer.trace("TEST WAS THIS VALUE:", value)
	return value
end

-- easier than a massive number of nil and {} tests
function eval.TRUE ()
	return true
end
-- returns a blank value
function eval.val (value)
	return value
end

function eval.create (args)
	-- args.kind, args.key

	-- don't accidentally use false
	if args.kind == nil then
		args.kind = X.keys[args.key] and X.keys[args.key].kind
		print("figured out kind", args.kind)
	end

	local x 
	if args.kind == "square" then
		x = zones.makeSquare(args.key)
		STATE.created_square = x
	elseif args.kind == "card" then
		x = card.create(args.key)
		STATE.created_card = x
	elseif args.kind == "token" then
		x = tokens.create(args.key)
		STATE.create_token = x
	else
		assert(false, "this kind won't work for create " .. tostring(args.kind))
	end
	
	print("made a thing", args.kind, args.key, x)
	STATE.created = x
	return x
end

function eval.put (args)
	--local thing, to, position = args.item, args.to, args.position
	
	-- shortcut for the [to] field in case it's just a string, look for a zone with that key
	if type(args.to) == "string" then
		args.to = zones.find(args.to)
	end
	
	assert(args.to)
	printer.trace("put", args.item, args.to, args.position)
	if args.item.kind == "card" then
		return zones.pushCard( args.to, args.item)
	elseif args.item.kind == "token" then
		return zones.pushToken( args.to, args.item)
	elseif args.item.kind == "square" then
		return zones.pushSquare( args.to, args.item, args.position)
	end
end

-- searches for multiple results unlike the simpler functions
-- inner variables do not accept functions (yet)
function eval.find ( args )
	local filters = {}
	table.insert(filters, entity.kindFilter(args.kind))
	table.insert(filters, entity.typeFilter(args.type))
	table.insert(filters, entity.keyFilter(args.key))
	table.insert(filters, zones.positionFilter(args.position))
	
	--table.insert(filters, zones.insideFilter(args.posX, args.posY))
	
	local result = zones.filtered(filters)
	STATE["referenced_multiple"] = result
	return result
end

function eval.findOne (args)
	return eval.find(args)[1] or false
end

--------------------- EVAL FUNCTIONS ABOVE ----------------------------------

-- receives a trigger and a list of params, which must match
local function confirm ( t, ...)
	return true
end

local function arrayLoop(t)
	local f, params = t[1], {}
	assert(eval[f], "ARR: no function found in " .. printer.pretty(t))
	for i, x in ipairs(t) do
		if type(x) == "table" then
			-- TODO: multi return?
			local res = evaluate(x)
			--printer.trace("evaluate returned", res)
			if res == false then
				printer.trace("Eval returned false", x)
				-- short cut the rest of the evaluation
				return false
			else
				table.insert(params, res)
			end
		elseif i > 1 then
			-- don't include function name in params
			table.insert(params, x)
		end
	end
	return f, eval[f], params
end

local function mapLoop(t)
	local f, params = t[1], {}
	assert(eval[f], "MAP: no function found in " .. printer.pretty(t))
	for k, v in pairs(t) do
		if type (v) == "table" then
			local res = evaluate(v)
			if res == false then
				printer.trace("Eval returned false", k)
				-- short cut the rest of the evaluation
				return false
			else
				params[k] = res
			end
		else
			params[k] = v
		end
	end
	return f, eval[f], params
end

-- stack unwind of a table where [1] is the function, and [2]+ are params. 
evaluate = function ( t )
	--printer.trace("evaluate called with", t)
	if not t then
		print("NIL PASSED TO EVALUATE, GIVING UP")
		return false
	end
	-- alternate execution for complex functions
	local result, key, f, params = false, nil, nil
	if t[2] then
		key, f, params = arrayLoop(t)
		if f then
			printer.trace("calling", key)
			result = f( unpack(params) )
		end
	else
		key, f, params = mapLoop(t)
		if f then
			printer.trace("calling", key)
			result = f( params )
		end
	end
	
	if  result then
		local events = possibleEvents(STATE.reactors, key, params)
		for _, x in ipairs(events) do
			table.insert(STATE.events, x)
		end
	end
	
	return result
end

-- deals with every detail of trying to execute an action by key 
local function attempt (action, interaction)
	-- only reset between actions, keep all state together for query + execution
	-- that way we can reference query results in the command step
	for k, v in pairs(interaction or {}) do
		STATE[k] = v
	end
	
	print("Checking action", action.key)
	-- some actions don't have queries, that's okay.
	local ok = eval.execute({ action = action, part = "query" })
	if not ok then
		return false
	end	
	print("Executing action", action.key)
	
	-- figure out what could react before executing the action, but after dealing with evaluate
	STATE.reactors = zones.reactors()
	
	-- discard results
	assert(action.command, "Action is incomplete " .. tostring(action.key))
	ok = eval.execute( { action = action, part = "command" })
	if ok ~= true then
		print("Execution of command failed", action.key)
		return false
	end
	
	return true
end

local function state()
	return STATE
end

-- register all actions for keyword-usage
local function init()
	for _, w in ipairs(X.actions) do
		assert(w.key, "Action missing key")
	end
end

-- replaces the eval require and runs every script
local function compile()
	local real_eval = eval
	eval = require("compiler")
	
	local eval = real_eval
end

return {
	attempt = attempt, 
	init = init,
	compile = compile,
	state = state,
	pushState = pushState,
	popState = popState,
}