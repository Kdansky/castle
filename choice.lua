-- displays all the things given to it with specific rules, such as
-- "discard the other cards" or "spawn a copy" or whatever. Mostly for CYOA or discover effects
-- Give it an action to create the choice, and another action to do with the result.
-- Cards or Squares or just plain text boxes should work. Produces an overlay.