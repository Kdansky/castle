local string = require("string")
local printer = require("printer")

local CONTENT = {}

local function singularAdd (k, v)
	CONTENT[k] = v
end

-- clean up entries from files
local KEYS = {}
local function compile (key, entry, kind)
	if KEYS[key] then
		assert(false, "Duplicate key detected: " .. key)
	end
	entry.kind = kind
	KEYS[key] = entry
	-- todo: Call compile functions per type so we can clean up mediocre declarations
	return entry
end

local function arrayAdd (coll, v, kind)	
	if not CONTENT[coll] then
		CONTENT[coll] = {}
	end
	local list = CONTENT[coll]
	
	-- merge the two arrays
	for _, entry in ipairs(v) do
		-- check for duplicate keys
		local key = entry.key
		local clean = compile (key, entry, kind)
		table.insert(list, clean)
	end
end

local TYPE_HANDLERS = {
	lookAndFeel = { singularAdd, "no" },
	actions = { arrayAdd, "action" },
	phases = {arrayAdd, "phase" },
	zones = {arrayAdd, "zone" },
	squares = {arrayAdd, "square" },
	tokens = {arrayAdd, "token" },
	cards = {arrayAdd, "card" },
	stats = {arrayAdd, "stat" },
}

local function all ( path )
	print("init content", path)
	local files = love.filesystem.getDirectoryItems( path .. "/logic" )
	
	for _, f in ipairs(files) do
		local s = path .. ".logic." .. string.gsub(f, '/','.')
		s = string.gsub(s, '%.lua$', '')
		-- require it
		local x = require(s)
		print("Loaded file", s)
		for kind, v in pairs(x) do
			local f = TYPE_HANDLERS[kind][1]
			assert(f, "Kind not defined " .. kind .. " in " .. s)
			f(kind, v, TYPE_HANDLERS[kind][2])
		end
	end
end

-- patch the init function into the content declaration 
CONTENT.init = function ( name )
	-- todo: might need to deal with multiple games?
	-- love.filesystem.setIdentity( path )
	CONTENT.path = name
	all( name )
	CONTENT.keys = KEYS
end


return CONTENT
