local X = require("declaration")
local entity = require("entity")
local printer = require("printer")
local card = require("card")
local assets = require("assets")
local stats = require("stats")
local texts = require("texts")
local zones = require("zones")

local TOOLTIP
local PLACES 
local MODE

local ENTITY = nil
local TEXTS = {}

local function resize()
	local margins = 
			TOOLTIP.margins or
			X.lookAndFeel.zones.margins or 4
	zones.basicPlace(PLACES.full, margins)
	zones.basicPlace(PLACES.text, margins)
	zones.basicPlace(PLACES.stats, margins)
	zones.basicPlace(PLACES.pic, margins)
	TEXTS = {}
end

local function recompute()
	-- nearly 
	TOOLTIP = entity.find(X.lookAndFeel.tooltips, MODE)
	PLACES = {
		full = {
			math.min (TOOLTIP.placeText[1], TOOLTIP.placePic[1], TOOLTIP.placeStats[1]),
			math.min (TOOLTIP.placeText[2], TOOLTIP.placePic[2], TOOLTIP.placeStats[2]),
			math.max (TOOLTIP.placeText[3], TOOLTIP.placePic[3], TOOLTIP.placeStats[3]),
			math.max (TOOLTIP.placeText[4], TOOLTIP.placePic[4], TOOLTIP.placeStats[4]),
		},
		text = TOOLTIP.placeText,
		stats = TOOLTIP.placeStats,
		pic = TOOLTIP.placePic,
	}
	resize()
end

local function getTTTStats ( stats )
	local result = {}
	for _, a in ipairs(stats) do
		local c = a.color or { 1, 1, 1 }
		table.insert(result, { a.key .. ": " .. a.value, c, "left" } )
	end
	return result
end

-- tooltiptext
local function assertTTTEntity ( e )
	if #TEXTS == 0 then
		-- text gets their own rectangle
		local place = PLACES.text
		table.insert( TEXTS,
				{
					bin = texts.make ( place.w, "center", e.key, { e.text, {1,1,1}, "left"}),
					x = place.x,
					y = place.y,
				})
		-- stats get their own window
		place = PLACES.stats
		local stats = getTTTStats (e.stats)
		table.insert( TEXTS,
				{
					bin = texts.make ( place.w, "left", unpack(stats)),
					x = place.x,
					y = place.y,
				})
	end
end


local function draw()
	if not ENTITY then
		return
	end
	local p = PLACES.full
	love.graphics.push("all")
	love.graphics.setBlendMode( "subtract", "premultiplied" )
	love.graphics.setColor(0.85, 0.85, 0.85)
	love.graphics.rectangle("fill", p.x, p.y, p.w, p.h)
	love.graphics.pop()
	
	love.graphics.push("all")
	love.graphics.setColor(1, 1, 1)
	if ENTITY.kind == "card" or ENTITY.kind == "square" or ENTITY.kind == "token" then
		local p = PLACES.pic
		assets.draw(ENTITY.asset, p.x, p.y, p.w, p.h)
		assertTTTEntity(ENTITY)
	end
	
	for _, t in ipairs(TEXTS) do
		love.graphics.draw(t.bin, t.x, t.y)
	end
	--printer.trace(PLACES)
	love.graphics.pop()
end

local PRIORITY = {
	token = 3,
	card = 2,
	square = 1,
	zone = -1,
}

-- choose the best thing to display a TT for
local function set( entities )
	local best = nil
	local bestValue = -0
	for _, e in ipairs(entities) do
		if PRIORITY[e.kind] > bestValue then
			best = e
			bestValue = PRIORITY[e.kind]
		end
	end
			
	if ENTITY ~= best then
		ENTITY = best
		TEXTS = {}
	end
end

local function nextMode()
	local n = entity.findSuccessor(X.lookAndFeel.tooltips, MODE)
	if not n then
		n = X.lookAndFeel.tooltips[1].key
	end
	MODE = n
	recompute()
end

local function init()
	MODE = X.lookAndFeel.tooltips[1].key
	recompute()
end

return {
	set = set,
	init = init,
	resize = resize,
	draw = draw,
	nextMode = nextMode
}
	
	
	