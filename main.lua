local X = require("declaration")
local card = require("card")
local zones = require("zones")
local game = require("game")
local assets = require("assets")
local entity = require("entity")
local stats = require("stats")
local tooltip = require("tooltip")
local eval = require("eval")
local texts = require("texts")
local printer = require("printer")

local STATE = "RUN"
local MOUSE = {}

-- Init
function love.load(arg)
	X.init(arg[1])
	love.window.setMode(X.lookAndFeel.video.width or 1280, X.lookAndFeel.video.height or 720, { vsync = -1, resizable = true, centered = true, minwidth = 160, minheight = 90})
	assets.init()
	eval.init()
	zones.init()
	card.init()
	game.init()
	tooltip.init()
	stats.init()
	texts.init()
end

-- main loop
local ddt = 10
local ttt = 0
function love.update(dt)
	if STATE == "QUIT" then
		love.event.quit( 0 )
	end
	
	-- move to resize callback
	ddt = ddt + dt
	if ddt > 1.5 or STATE == "REDRAW" then
		tooltip.resize()
		zones.resize("force")
		ddt = 0
		STATE = "RUN"
	else
		-- deal with resize themselves
		zones.resize()
	end
	
	-- if mouse over stuff then
	if MOUSE.x then
		tooltip.set({})
		ttt = 0
	else
		-- some simplified mouse-over delay logic so it's not too annoying.
		local x, y = love.mouse.getPosition()
		local show = zones.locate(x, y)
		if #show == 0 then
			ttt = 0
		elseif ttt > 0.6 then
			tooltip.set(show)
		end
		ttt = ttt + dt
	end
end

local function drawBG()
	love.graphics.push("all")
	love.graphics.setColor(1.8,1.8,1.8)
	local h, w = love.graphics.getDimensions( )
	assets.draw(X.lookAndFeel.background.asset, 0, 0, h, w, "force")
	love.graphics.pop()
end

local function drawMouse()
	local x, y = love.mouse.getPosition()
	if MOUSE.x and MOUSE.y and entity.distance(MOUSE.x, MOUSE.y, x, y) > 10 then
		love.graphics.setLineStyle( "smooth" )
		love.graphics.push("all")
		love.graphics.setLineWidth( 5 )
		love.graphics.line(MOUSE.x, MOUSE.y, x, y) 
		love.graphics.pop()
	end
end

function love.mousepressed(x, y, button, istouch)
	MOUSE.x = x
	MOUSE.y = y
end

function love.mousereleased(x, y, button, istouch)
	if MOUSE.x and MOUSE.y and entity.distance(MOUSE.x, MOUSE.y, x, y) > 10 then
		game.mouseDragged(MOUSE.x, MOUSE.y, x, y)
	else
		game.mouseClicked(x, y, button)
	end
	MOUSE = {}
end

function love.keyreleased(key)
	if love.keyboard.isDown( "lalt" ) then
		if key == "q" then
			STATE = "QUIT"
		elseif key == "w" then
			love.window.setFullscreen(not love.window.getFullscreen())
			STATE = "REDRAW"
		end
	elseif key == "tab" then
		tooltip.nextMode()
	else	
		game.keyClicked(key)
	end
end

function love.keypressed(key)

end

function love.draw()
	drawBG()
	zones.draw()
	card.draw()
	tooltip.draw()
	drawMouse()
	--local x, y = love.mouse.getPosition()
	--local w, h = love.graphics.getDimensions( )
	--love.graphics.print("Coordinate of mouse " .. tostring(x) .. "/" .. tostring(y) , 100, 100)
end


function love.focus(f)
	if not f then
		print("LOST FOCUS")
	else
		print("GAINED FOCUS")
		STATE = "REDRAW"
	end
end

