-- don't write util modules.

local function deepcopy (orig)
    local copy = orig
    if type(orig) == 'table' then
        copy = {}
        for k, v in next, orig, nil do
            copy[k] = deepcopy(v)
        end
    end
    return copy
end

-- returns true if a given entity has a type
-- should probably cache this at some point for performance
local function hasType (e, _type)
	for _, t in ipairs(e.types) do
		if t == _type then
			return true
		end
	end
	return false
end

-- multi OR type checking
local function hasAnyType(e, types)
	local ts = {}
	for _, t in ipairs(types) do
		ts[t] = true
	end
	
	for _, t in ipairs(e.types) do
		if ts[t] then
			return true
		end
	end	
	return false
end

-- space can either be a pointer or a name
local function find (collection, entity_or_name, kind)
	assert(entity_or_name, "no name given to search")
	for _, e in ipairs(collection) do
		-- debugging
		assert(not kind or e.kind == kind)
		if e == entity_or_name or e.key == entity_or_name then
			return e
		end
	end
	return false
end

local function findSuccessor (collection, entity_or_name, kind)
	assert(entity_or_name, "no name given to seearch")
	for i, e in ipairs(collection) do
		if e == entity_or_name or e.key == entity_or_name and collection[i+1] then
			return collection[i+1]
		end
	end
	assert(false, tostring(entity_or_name) .. " not found")
	return false
end

local function distance ( x1, y1, x2, y2)
	return math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
end

local function shuffle ( collection )
	for i = #collection, 2, -1 do
		local j = math.random(i)
		collection[i], collection[j] = collection[j], collection[i]
	end
	return collection
end

local function kindFilter ( kind )
	return kind and function ( e )
		return e.kind == kind
	end
end

local function typeFilter ( aType )
	return aType and function ( e )
		return hasType(e, aType)
	end
end

local function keyFilter ( key )
	return key and function (e)
		return e.key == key
	end
end

return {
	copy = deepcopy,
	hasType = hasType,
	hasAnyType = hasAnyType,
	find = find,
	findSuccessor = findSuccessor,
	distance = distance,
	shuffle = shuffle,
	kindFilter = kindFilter,
	typeFilter = typeFilter,
	keyFilter = keyFilter,
}