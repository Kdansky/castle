local string = require("string")
local table = require("table")
local json = require("dkjson")

-------------------------------------------------------
-- Output functionality for dkjson
local keynames = {}

-- t can have subtables. eh.
local function addToOrderTable( t )
  -- add every key found.
  for key, val in pairs(t) do
    if not keynames[key] then
		keynames[key] = true
    end
    if type(val) == "table" then
		addToOrderTable(val)
    end
  end
end

local function convertKeynamesToOrder()
  local sorter = {}
  for key, val in pairs(keynames) do
    -- do not add array indices
    if type(key) == "string" then
		table.insert(sorter, key)
    end
  end

  table.sort(sorter)
  return sorter
end

-- Generates a sorted json string from a table
local function jsonEncodeSorted ( val )
  addToOrderTable(val)
  local sorter = convertKeynamesToOrder()
  return tostring(json.encode(val, { indent = true, keyorder = sorter }))
end

-- return a printable string
local function pretty(...)
	local buffer = {}
	for n=1, select('#', ...) do
		local val = select(n, ...)
		if type(val) == "table" then
			table.insert( buffer, jsonEncodeSorted(val))
		else
			table.insert( buffer, tostring(val))
		end
		table.insert( buffer, "\t" )
	end
	return table.concat(buffer, "  ")
end

-- print a printable string
local function trace( ... )
    print(pretty(...))
end

-- split string into two substrings at a speratator, 
local function split( s, sep )
	local index = string.find(s, sep)
	if index then
		local key = string.sub(s, 1, index-1)     	-- e.g. "Authorization"
		local val = string.sub(s, index, #s)		-- e.g. ":Basic ASDF"
		return key, val
	end
	return s, ""
end

-- prints a pretty table (spaces defaults to 4)
local function tablePrint ( t, spaces )
	local characters = {}
	for _, line in ipairs(t) do
		for i, word in ipairs(line) do
			local len = tostring(word):len()
			if not characters[i] or len > characters[i] then
				characters[i] = len
			end
		end
	end
	
	local buf = {}
	-- now we know the correct column widths
	for _, line in ipairs(t) do
		for i, rawWord in ipairs(line) do
			local word = tostring(rawWord)
			local len = word:len()
			table.insert(buf, word)
			
			-- pad unless this is the last column
			if i < #line then
				-- pad
				while len < characters[i] + ( tonumber(spaces) or 4 ) do
					table.insert(buf, " ")
					len = len+1
				end
			end
		end
		table.insert(buf, "\n")
	end
	local s = table.concat(buf, "")
	if #s > 0 then
		print(s)
	end
end

return {
	trace = trace,
	split = split,
	tablePrint = tablePrint,
	pretty = pretty,
}