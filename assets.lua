local X = require("declaration")

local ASSETS = {}

local function loadImage (name)
	if type(name) == "string" and not ASSETS[name] then
		print("loading image", name)
		ASSETS[name] = love.graphics.newImage(X.path .. "/assets/" .. name)
	end
end

local function loadAssets (t)
	for k, v in pairs(t) do
		if k == "asset" then
			loadImage(v)
		elseif type(v) == "table" then
			loadAssets(v)
		end
	end
end

local function init ()
	loadAssets(X)
end

local function drawable (asset)
	return ASSETS[asset]
end

local SCALINGS = {
	best = function ( ass, w, h)
		local iw, ih = ass:getDimensions()
		-- assume original max size if it is not given
		local size = math.min( w / iw, h / ih )
		return size, size
	end,
	force = function (ass, w, h)
		local iw, ih = ass:getDimensions()
		return w / iw, h / ih
	end
}

-- find best sizing factor to conform to given space


-- resize a picture to fit a given frame
-- mode = nil, force, 
local function draw (asset, x, y, w, h, mode)
	local i = ASSETS[asset]
	local scaleX, scaleY = SCALINGS[mode or "best"](i, w, h)
	love.graphics.draw(i, x, y, 0, scaleX, scaleY)
end

return {
	init = init,
	drawable = drawable,
	draw = draw,
	ASSETS = ASSETS,
}