GENERAL

Naming convention: Names that are all-lowercase with underscores reference stock behaviour. Users should use title-style text like "The Fireball Spell" to avoid name-clashing.

key: Every entity has a key which it is referenced by. These are strings.
text: A user-readable text attached to the entity.
types: Every entity has a list of types that define its basic behaviour.
keywords: 
	If the entity is evaluated for query/command, its keywords are checked, and all q/c for any keyword is also checked or executed.
	This allows declaration of something like mana cost only once, and then reference it on every card that is played with just one word (like "Spell")
color: Rendering information
asset: Picture to use for the entity

PHASES
types: 
	- repeats: This phase repeats until ended.
	- automatic: All possible actions in this phase are executed automatically when the phase starts.
	- manual_end: The phase ends when an action calls the "phase_end" function

ACTIONS
query: The query is executed and checked if it results in true. Only then will the command be excuted. Absence of a query is treated as TRUE.
command: The command to execute


SPACES
types:
	- hand: A player hand. Can contain cards, no specific ordering.
	- board: A play board. Can contain squares.
		- grid: Requires board. Decides the layout of the board.
	- deck:	A deck of cards.
	- hidden: Will not be rendered.
	- stats: Contains a list of stats.

SQUARES



STATS
max: Maximum value the stat can have.
min: Minimum value the stat must have.
types:
	- ignore_overflow: Going above the maximum just clamps, but is accepted. By default the change would be rejected.
	- ignore_underflow: Going below the minimum just clamps, but is accepted. By default the change would be rejected.
triggers: 
	- min: When the value goes to or below min, the given action is executed
	- max: When the value goes to or above max, the given action is executed
	

FUNCTIONS

create ( key, kind )
	Produces a new instance of any entity by given key and kind

put ( entity, to, position )
	Moves any given entity from wherever it is to the [to]. This sometimes requires position information (such as grid coordinates)
	
find ( key, type, kind, position )
findOne ( key, type, kind, position )
	Finds all entities that match all given criteria. FindOne only returns the first match, find returns all matches.












	