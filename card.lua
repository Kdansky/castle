local assets = require("assets")
local X = require("declaration")
local entity = require("entity")
local printer = require("printer")
local stats = require("stats")

-- lists all cards that are instantiated
local CARDS = {}

local function init()
end

local COLORS = {
	legendary = { 1, 0.65, 0.01},
	epic = { 0.65, 0.20, 0.92},
	rare = { },
	uncommon = {},
	common = {},
}

local function drawOneSmall (card)
	love.graphics.push("all")
	-- frame
	local p = card.place
	love.graphics.setColor(1, 0.65, 0.01)
	love.graphics.rectangle("line", p.x, p.y, p.w, p.h, 5, 5)
	love.graphics.rectangle("line", p.x+1, p.y+1, p.w-2, p.h-2, 5, 5)
	love.graphics.setBlendMode( "subtract", "premultiplied" )
	love.graphics.setColor(0.6, 0.6, 0.6)
	love.graphics.rectangle("fill", p.x, p.y, p.w, p.h)
	
	-- TODO: do some proper scaling here later
	local newY = p.y + p.h*2/3
	
	love.graphics.setBlendMode( "alpha" )
	love.graphics.setColor(1, 1, 1)
	assets.draw(card.asset, p.x+3, p.y+3, p.w-6, p.h*2/3 -6)
	love.graphics.printf(card.key, p.x+3, newY, p.w-6, card.layout or "center")
	love.graphics.pop()
end

-- mostly for tooltips
local function drawFullCard (card, x, y, w, h)
	love.graphics.push("all")
	love.graphics.setColor(1, 0.65, 0.01)
	love.graphics.rectangle("line", x, y, w, h, 5, 5)
	love.graphics.rectangle("line", x+1, y+1, w-2, h-2, 5, 5)
	love.graphics.setBlendMode( "subtract", "premultiplied" )
	love.graphics.setColor(0.6, 0.6, 0.6)
	love.graphics.rectangle("fill", x, y, w, h)
	
	-- TODO: do some proper scaling here later
	local newY = y + h/3
	
	love.graphics.setBlendMode( "alpha" )
	love.graphics.setColor(1, 1, 1)
	assets.draw(card.asset, x+3, y+3, w-6, h/3 -6)
	love.graphics.printf(card.key, x+3, newY, w-6, card.layout or "center")
	love.graphics.printf(card.text, x+3, newY+30, w-6, card.layout or "center")
	love.graphics.pop()
end

local function draw()
	for _, c in ipairs(CARDS) do
		if c.place then
			drawOneSmall(c)
		end
	end
end

local function create (key)
	local card = entity.copy( entity.find(X.cards, key) )
	stats.fill(card)
	-- TODO: decide on where that card actually is, all cards need to have a place
	table.insert(CARDS, card)
	return card
end

return {
	init = init,
	draw = draw,
	drawFullCard = drawFullCard,
	create = create,
}