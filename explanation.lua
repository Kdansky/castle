-- returns a readable description for any action
local X = require("declaration")

local expl = {}

----------------------------- Events -------------------------------

function expl.reactor ()	return "The reacting object"	end

function expl.set ( key, value )
	return "Write '" .. value .."' into a variable '" .. key .. "'"
end

function eval.get ( key )
	return "Read the value of the variable '" .. key "'"
end

function eval.event_reference ( key )
	return "Read the value of the variable '" .. key "' from the original event"
end

-- Runs multiple commands, discarding all return values
function eval.multi (...)
	return "Run all the following commands: " .. table.concat({...}, ";") .. " and ignore the return values"
end

-- Runs multiple commands, returning the AND of all return values
function eval.all (...)
	return "Run all the following commands: " .. table.concat({...}, ";") .. " and check all the return values"
end

-- Repeats the same action multiple times
function eval.loop ( number, ...)
	return "Run the following command: " .. table.concat({...}, " ") .. tostring(number) .. " times"
end

function eval.space_at (key, position, player)
	return "Find a space"
end

---------------------------------Squares---------------------------------
-- creates a square from key + anEntity structure
function eval.create_square ( squareKey, anEntity, position )
	return "Create a square with key " .. squareKey 
end

-- removes a square at the given position
function eval.remove_square ( position )
	return "Remove a square at position" .. position
end

-- finds the square that matches the position and optionally key
function eval.square_at (key, position, player)
	return "Find a square"
end

-- checks if a square is in a given space
function eval.square_in ( key, aSquare )
	return "This square: '".. aSquare .."' is inside a space with key " .. key
end

-- finds the next square in a given cardinal direction (N, S, W, E)
function eval.square_dir ( aSquare, direction )
	assert(0, "todo")
end

-----------------------------cards---------------------------------

-- creates a new card in a given entity (must be a space)
function eval.create_card ( cardKey, anEntity )
	return "Create a card " .. cardKey
end

function eval.push_card ( aCard, anEntity )
	return "Put a card into place"
end

-- returns the top card of a deck (or hand)
function eval.top_card ( key )
	return "The top card of a deck"
end

-- huh, same thing?
function eval.card_at ( key )
	assert(false, "MISSING FUNCTIONALITY")
	return false
end

-- returns the top-most card at a mouse-pos
function eval.any_card_at ( position )
	return "The card at mouse position"
end

function eval.delete_card (  )
	assert(false)
end

-- fills a deck with cards and then shuffles it
-- technically could be done through a combination of more basic actions?
function eval.fill_deck ( aDeck, ... )
	return "Fill a deck with cards"
end

-----------------------------tokens------------------------------------

function eval.create_token ( key, aSquare )
	return "Create a token " .. key
end

-- returns true if a token can be put on a square (could be better)
function eval.token_square ( aSquare )
	return "Check if a square can contain tokens"
end

function eval.token_at ( spaceKey, position, player )
	return "Token at a position"
end

---------------------------entities------------------------------------

function eval.entity_type ( anEntity, aType )
	return "Entity has a type"
end

function eval.copy_asset (from, to)
	return "Copy the asset of " .. from .. " to " .. to
end

-- runs the query of an entity to see if the entity can be played
function eval.entity_query ( anEntity )
	return "Check the query of an entity " .. anEntity
end

-- activates the command of an entity
function eval.entity_command (anEntity)
	return "Run the command of an entity " .. anEntity
end

function eval.entities_not_equal ( a, b )
	assert(type(a) == "table")
	assert(type(b) == "table")
	return a ~= b
end

function eval.entities_equal ( ... )
	local t = {...}
	if not #t > 1 then
		return false
	end
	local prev = t[1]
	for _, v in ipairs(t) do
		if v ~= prev then
			return false
		end
		prev = v
	end	
	return true
end	

-------------------------- references ---------------------------------
-- could be done with a "state" function, but that complicates the declaration...
function eval.referenced_card ()	return STATE["referenced_card"] or false	end
function eval.referenced_square()	return STATE["referenced_square"] or false	end
function eval.referenced_token()	return STATE["referenced_token"] or false	end
function eval.created_card ()	return STATE["created_card"] or false	end
function eval.created_square ()	return STATE["created_square"] or false	end
function eval.created_token ()	return STATE["created_token"] or false	end

function eval.referenced_multiple (num) 
	return STATE["referenced_multiple"] and 
			STATE["referenced_multiple"][num] or false
end

function eval.reactor()	return STATE["reactor"] or false	end
function eval.self ()	return STATE["self"] or false	end


function eval.counting_cards_or_squares_someow_and_comparignthem ( position )
	-- TODO: figure out an elegant way of doing this
	assert(false)
	return false
end

function eval.param ( name )
	return STATE[name] or false
end

function eval.event_param ( name ) 
	return OLD_STATE[name] or false
end

-------------------------- positions ---------------------------------
-- produces a position struct
function eval.gridpos (x, y)
	return { kind = "gridXY", x = x, y = y}
end
	
-- produces a position struct
function eval.mouse_position ()
	local x, y = love.mouse.getPosition()
	STATE["mousepos"] = { x = x, y = y }
	return { kind = "screenXY", x = x, y = y}
end

------------------------ UI interactions--------------------------------
-- produces a position struct
function eval.mouse_dragged_from ()
	local d = STATE["drag"]
	if not d or not #d == 4 then
		return false
	end
	return { kind = "screenXY", x = d[1], y = d[2] }
end

-- produces a position struct
function eval.mouse_dragged_to ()
	local d = STATE["drag"]
	if not d or not #d == 4 then
		return false
	end
	return { kind = "screenXY", x = d[3], y = d[4] }
end

-- check if we are clicking
function eval.rmb_clicked ()
	return STATE["rmb"] or false
end

-- check if we are clicking
function eval.lmb_clicked ()
	return STATE["lmb"] or false
end

function eval.key_clicked ( button )
	return STATE["key"] == button
end

-- call a different action
function eval.action_command ( key )
	assert(key, "Execute Action needs a key")
	local a = entity.find(X.actions, key)
	return evaluate( a.command )
end

-- call a different action
function eval.action_query ( key )
	local a = entity.find(X.actions, key)
	return evaluate( a.query )
end

-- Checks if a drag was between two entities
function eval.drag ( from_entity, to_entity )
	local d = STATE["drag"]
	if not d or not #d == 4 then
		return false
	end
	local fromSpaces = space.locate( d[1], d[2] )
	local toSpaces = space.locate( d[3], d[4] )
	
	for _, from in ipairs( fromSpaces ) do
		if from == from_entity then
			for _, to in ipairs( toSpaces ) do
				if to == to_entity then
					return true
				end
			end
		end
	end
	return false
end

---------------------------Stats---------------------------------

function eval.create_stat ( statKey, anEntity, val )
	local stat = stats.create( anEntity, statKey, val)
	STATE["created_stat"] = stat
	return stat
end

-- works, but very repetitive?
function eval.stat_can_decrease ( statKey, anEntity, value)
	return stats.canAfford( anEntity, statKey, -1*value )
end
function eval.stat_decrease (statKey, anEntity, value)
	return stats.change( anEntity, statKey, -1*value )
end
function eval.stat_increase (statKey, anEntity, value)
	return stats.change( anEntity, statKey, value )
end

function eval.stat_property ( statKey, anEntity, property )
	assert(property == "max" or property == "mind" or property == "value")
	return stats.find ( anEntity, statKey )[property] or false
end

function eval.copy_stat ( statKey, fromEntity, toEntity )
	return stats.copy (fromEntity, toEntity, statKey)
end

-- applies a change to multiple entities, returns true if at least one application was successful
function eval.multi_stat ( stat_function, statKey, someEntities, value)
	local result = false
	for _, e in ipairs(someEntities) do
		result = eval[stat_function](statKey, e, value) or result
	end
	return result
end

---------------------------Phases---------------------------------

function eval.end_phase ()
	return phase.nextPhase()
end

function eval.activate_phase (key)
	local actions = phase.activatePhase(key)
	STATE["activated_phase"] = key
	while actions and #actions > 0 do
		for _, a in ipairs(actions) do
			eval.action_command ( a )
			actions = phase.nextPhase()
		end
	end
end

function eval.phase_is (key)
	return phase.get().key == key
end

---------------------------Filler---------------------------------
-- always returns true but noisly ()
function eval.test ( value )
	printer.trace("TEST WAS THIS VALUE:", value)
	return value
end

-- easier than a massive number of nil and {} tests
function eval.TRUE ()
	return true
end
-- returns a blank value
function eval.val (value)
	return value
end

-- searches for multiple results unlike the simpler functions
-- inner variables do not accept functions (yet)
function eval2.find ( args )
	local filters = {}
	table.insert(filters, entity.kindFilter(args.kind))
	table.insert(filters, entity.typeFilter(args.type))
	--table.insert(filters, space.insideFilter(args.posX, args.posY))
	
	local result = space.filtered(filters)
	STATE["referecend_multiple"] = result
	return result
end

function eval2.findOne (args)
	return eval2.find(args)[1] or false
end

--------------------- EVAL FUNCTIONS ABOVE ----------------------------------

local function newEval (t)
	local f, params = eval2[t.f], {}
	for k, v in ipairs(t) do
		if type(v) == "table" then
			local res = evaluate(v)
			params[k] = res
		else
			params[k] = v
		end
	end
	return f(params)
end

-- receives a trigger and a list of params, which must match
local function confirm ( t, ...)
	return true
end

-- stack unwind of a table where [1] is the function, and [2]+ are params. 
evaluate = function ( t )
	--printer.trace("evaluate called with", t)
	if not t then
		print("NIL PASSED TO EVALUATE, GIVING UP")
		return false
	end
	-- alternate execution for complex functions
	if t.f then
		return newEval(t)
	end
	local f, params = nil, {}
	for i, x in ipairs(t) do
		if i == 1 then
			f = x
			assert(f, "no function found in " .. printer.pretty(t))
		else
			if type(x) == "table" then
				-- TODO: multi return?
				local res = evaluate(x)
				--printer.trace("evaluate returned", res)
				if res == false then
					printer.trace("Eval returned false", x)
					-- short cut the rest of the evaluation
					return false
				else
					table.insert(params, res)
				end
			else
				table.insert(params, x)
			end
		end
	end
	assert(t[1], "Missing function")
	printer.trace("calling", t[1])
	
	-- before calling f, check if that would trigger anything
	local events = possibleEvents(STATE.reactors, f)

	local result = eval[f]( unpack(params))
	
	--[[ for the moment we just ignore the part where we need to make sure it's a match
	for _, r in ipairs(reactions) do
		-- if confirm (r, unpack(params)) then -- valid reaction, we need to execute this afterwards
	end ]]
	-- there can be events already in the array, just add them
	for _, x in ipairs(events) do
		table.insert(STATE.events, x)
	end
	
	return result
end

-- deals with every detail of trying to execute an action by key 
local function attempt (action, interaction)
	-- only reset between actions, keep all state together for query + execution
	-- that way we can reference query results in the command step
	for k, v in pairs(interaction or {}) do
		STATE[k] = v
	end
	
	print("Checking action", action.key)
	-- some actions don't have queries, that's okay.
	local ok = evaluate(action.query or {"TRUE"})
	if not ok then
		return false
	end	
	print("Executing action", action.key)
	
	-- figure out what could react before executing the action, but after dealing with evaluate
	STATE.reactors = space.reactors()
	
	-- discard results
	assert(action.command)
	ok = evaluate( action.command )
	if ok ~= true then
		print("Execution of command failed", action.key)
		return false
	end
	
	return true
end

local function state()
	return STATE
end

-- register all actions for keyword-usage
local function init()
	for _, w in ipairs(X.actions) do
		assert(w.key, "Action missing key")
	end
end

-- replaces the eval require and runs every script
local function compile()
	local real_eval = eval
	eval = require("compiler")
	
	local eval = real_eval
end

return {
	attempt = attempt, 
	init = init,
	compile = compile,
	state = state,
	pushState = pushState,
	popState = popState,
}