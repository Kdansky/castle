-- is for rendering cached text strings
local X = require("declaration")
local printer = require("printer")

local FONT
local function init ()
	FONT = love.graphics.newFont("/fonts/" .. X.lookAndFeel.font.name, X.lookAndFeel.font.size, "normal")
end

-- takes an list of strings and produces a text object for it
local function make ( width, alignment, ... )
	--printer.trace("making text", width, alignment)
	local t = love.graphics.newText( FONT, "" )
	local y = 0
	for _, s in ipairs({...}) do
		if type(s) == "string" then
			local index = t:addf (s, width, alignment, nil, y)
			y = y + t:getHeight(index)
		else
			assert(type(s) == "table")
			-- s[1] = string, s[2] = color, s[3] = alignment
			--printer.trace("addf", s, t:getHeight())
			local index = t:addf ( {s[2] or {1,1,1}, s[1]}, width, s[3] or alignment, nil, y)
			y = y + t:getHeight(index)
		end
	end
	return t
end

return {
	make = make,
	init = init
}