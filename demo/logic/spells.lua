return {
	actions = {
		{
			-- for any action taken by an entity with a matching keyword tag
			key = "Spell",
			query = { "stat_can_decrease", "Mana", {"zone_at", "Player Statistics" }, { "stat_property", "Mana", {"referenced_card"}, "value" } },
			command = { "all" ,
				{ "stat_decrease", "Mana", {"zone_at", "Player Statistics" }, { "stat_property", "Mana", {"referenced_card"}, "value" } },
				{ "push_card", { "referenced_card" }, { "zone_at", "Graveyard" } },
			},
		},
		{
			key = "Summon",
			query = {"token_square", { "square_at", "any", { "mouse_position" }}},
			command = { "put", item = {"create", key = {"get", "Summon Key"}}, to = {"referenced_square"}}
		},
		{	-- deals damage to a token
			key = "Nuke",
			query = { "all", 
				{"token_at", "Battlefield", {"mouse_position"}}, 
				{"stat_can_decrease", "Health", {"referenced_token"}, {"stat_property", "Attack", {"referenced_card"}, "value" } }
			},
			command = { "stat_decrease", "Health", {"referenced_token"}, {"stat_property", "Attack", {"referenced_card"}, "value" } },
		},
		{
			key = "Play Card",
			query = { "all", 
				{ "drag", {"zone_at", "Hand"}, {"zone_at", "Battlefield"}},
				{ "entity_query", { "any_card_at", {"mouse_dragged_from"}} }},
			-- need to check if it can be a card, and also need to check if the card's query is fulfilled
			command = { "entity_command", { "referenced_card" }},
		},
	},
	squares = {
		{
			key = "Add Mana Button",
			types = { "Button" },
			asset = "witch.jpg",
			text = "Gives you 3 Mana.",
			command = { "stat_increase", "Mana", {"zone_at", "Player Statistics" }, 3 }
		},
		{
			key = "Draw Card Button",
			types = { "Button" },
			asset = "teacher.jpg",
			text = "Draws a card.",
			command = { "push_card", {"top_card", "Deck" }, {"zone_at", "Hand"}}
		}
	},
	cards = {
		{
			key = "Fireball",
			asset = "fireball.png",
			text = "This spell nukes a Token!",
			style = "legendary",
			types = {},
			actions = { "Spell", "Nuke" },
			stats = { Mana = 3, Attack = 3 },	-- gets replaced with a full node during init
		},
	},
	stats = {
		{
			key = "Mana",
			text = "How much mana you can spend on spells",
			min = 0,
			max = 10,
			types = { "integer", "allow_overflow" },
			asset = "staff.png",
			color = { 0, 0.8, 1 },
		},
	}
}
