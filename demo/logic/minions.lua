return {
	squares = {
		{
			key = "Dragon",
			--color = { 0.7, 0, 0.2 },
			types = {},
			asset = "dragon.jpg",
			-- need a way to add formatted text not inline
			text = "Gives tokens that enter this space +1 Health.",
			reactions = {
				{
					trigger = { "Summon" },
					-- unit entered *this* square. Ugh. I think I just need to hardcode a ton of events
					query = { "entity_equal", {"reactor"}, {"event_reference", "created_token"} },
					command = { "stat_increase", "Health", {"reactor"}, 1 }
				},
			}
		}
	},
	stats = {
		{
			key = "Attack",
			text = "Attack damage",
			min = 0,
			types = { "integer", "ignore_underflow" },
			asset = "hammer.png",
			color = { 0.9, 0.2, 0.1 },
		},
		{ 
			key = "Health",
			text = "This is health number",
			max = 100,
			types = { "integer", "ignore_overflow" },
			asset = "health.png",
			triggers = { "Die At 0 Health" },
			color = { 0.2, 0.6, 0.1 },
		},
	},
	tokens = {
		{
			key = "Vampire",
			text = "A vampire",
			asset = "vampire.jpg",
			types = { "creature" },
			stats = { Attack = 1, Health = 1, Ready = 0 },
		},
	},
	cards = {
		{
			key = "Summon Vampire",
			asset = "vampire.jpg",
			text = "Summon a Vampire.",
			stats = { Mana = 1 },
			command = { "set", "Summon Key", "Vampire" },	-- this value is later used by Summon
			types = {},
			actions = { "Spell", "Summon" },
		},
	}
}
