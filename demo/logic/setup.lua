return {
	actions = {
		{	-- setup of everything
			key = "Setup The Field",
			command = { "all", 
			--battlefield
				--{ "create_square", "Dragon", {"zone_at", "Battlefield"}, {"gridpos", 1, 1}},
				--{ "put", item = {"create", kind="square", key="Dragon" }, to={"zone_at", "Battlefield"}, position ={"gridpos", 1, 1}},
				--{ "put", item = {"create", key="Dragon"}, to={"zone_at", "Battlefield"}, position ={"gridpos", 1, 1}},
			
				{ "put", item = {"create", key="Dragon"}, to="Battlefield", position ={"gridpos", 1, 1}},
			
				{ "create_square", "Dragon", {"zone_at", "Battlefield"}, {"gridpos", 2, 1}},
				{ "create_square", "Dragon", {"zone_at", "Battlefield"}, {"gridpos", 3, 1}},
				{ "create_square", "Place", {"zone_at", "Battlefield"}, {"gridpos", 1, 2}},
				{ "create_square", "Place", {"zone_at", "Battlefield"}, {"gridpos", 2, 2}},
				{ "create_square", "Place", {"zone_at", "Battlefield"}, {"gridpos", 3, 2}},
				{ "create_stat", "Mana", {"zone_at", "Player Statistics"}, 0},
			-- menu
				{ "create_square", "Add Mana Button", {"zone_at", "Menu"}, {"gridpos", 1, 1}},
				{ "create_square", "Draw Card Button", {"zone_at", "Menu"}, {"gridpos", 2, 1}},
				{ "create_square", "End Turn", {"zone_at", "Menu"}, {"gridpos", 3, 1}},
			-- deck
				{ "fill_deck", {"zone_at", "Deck" }, "Warlock", 4, "Fireball", 2, "Summon Vampire", 4 },
				{ "execute", key = "Draw Card", count = 7},
			}
		},
		{
			key = "Draw Card",
			command = { "push_card", {"top_card", "Deck" }, {"zone_at", "Hand"}}
		},
		{
			key = "Refresh Mana",
			command = { "stat_increase", "Mana", { "zone_at", "Player Statistics" }, 10},
		},
		{
			key = "Refresh Ready",
			command = {"multi_stat", "stat_increase", "Ready", { "find", kind = "square", type = "Button" }, 1},
		},
	}
}