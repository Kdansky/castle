return {
	actions = {
		{	-- testing action
			key = "RClick To Create A Dragon",
			query = { "rmb_clicked", { "zone_at", "Battlefield", {"mouse_position"} } },
			command = { "action_command", "Create A Dragon", }
		},
		{
			key = "Click To Delete A Dragon",
			query = { "lmb_clicked", { "square_at", "any", {"mouse_position"}} },
			command = { "remove_square", {"mouse_position"} },
		},
		{
			key = "Create A Dragon",
			command = { "create_square", "Dragon", {"zone_at", "Battlefield"}, {"mouse_position"}},
		},
		{
			key = "Activate A Button",
			query = {"all",
				-- these checks might be overkill
				{ "lmb_clicked", { "square_at", "any", {"mouse_position"}} },
				{ "entity_type", {"zone_at", "Controls", {"mouse_position"}}, "Menu"},
				{ "entity_type", {"referenced_square"}, "Button"},
				{ "entity_query", { "referenced_square" }},
			},
			command = { "entity_command", { "referenced_square" }},
		},		
		{
			key = "Keyboard: A",
			query = { "key_clicked", "a" },
			command = { "create_card", "Warlock", {"zone_at", "Hand" }},
		},
		{
			key = "Keyboard: D",
			query = { "key_clicked", "d" },
			command = { "stat_increase", "Mana", { "zone_at", "Player Statistics" }, 1},
		},
		{
			-- does not yet work (TODO)
			key = "Creature Attack",
			query = { "all",
				{ "drag", { "token_at", "Battlefield", {"mouse_dragged_from"}, "active" }, { "token_at", "Battlefield", {"mouse_dragged_to"}, "other" }},
				{ "query_token", false}
			}
		},
		{	-- kill a token when its Health reaches 0 or less (TODO)
			key = "Die At 0 Health",
			command = { "stat_container", "self" }, -- kill owner of this stat
		},
	}
}
