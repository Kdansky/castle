return {
	tokens = {
		{
			key = "Wizening Warlock",
			text = "A growing creature",
			asset = "warlock.jpg",
			types = { "creature" },
			stats = { Ready = 0, Attack = 1, Health = 1,},
			reactions = { 
				{
					-- trigger is executed when first function is called, then checks given params immediately for match
					-- issue: does not quite work because the kind value is often empty
					trigger = { "create", kind = "square" },
					-- query/command work as usual, except we are now in the event state
					query = { "TRUE" },
					command = { "stat_increase", "Health", {"reactor"}, 1 }
				},
				{
					trigger = { "entity_command" },
					-- or possibly execute-action + play card? Needs extra checking in either case
					-- trigger checking logic must run by reading STATE, but must also not modify it.
					-- needs to prevent our own card from triggering this. old.self = own card, self = new card
					-- during event execution we want to write to NEW_STATE, but maybe sometimes read from OLD_STATE?
					query = { "entities_equal", { "event_param", "self" }, { "event_param", "referenced_card"} },
					-- issue: these references are in the wrong context, and the running query could change them via
					-- square_at or similar. That would be an issue.
					command = { "stat_increase", "Attack", {"reactor"}, 1 }
				},
			}
		},
	},
	cards = {
		{
			key = "Warlock",
			asset = "warlock.jpg",
			text = "Summon a Warlock which gains +1 Health whenever another creature is summoned, and one Attack whenever a spell is cast.",
			stats = { Mana = 4 },
			types = {},
			command = { "set", "Summon Key", "Wizening Warlock" },
			actions = { "Spell", "Summon" },
			-- not ideal yet, I need a way to add a query
		},
	}
}
--[[
I would like triggers to be defined as simply as possible, within the thing that
reacts. It is more important to have easy declaratino than easy implementation.
Trigger examples:
	* Hp < 0		-> death (stat-specific)
	* Deathrattle	-> ...
	* Battlecry		-> ...
	* Whenever X comes into play	-> ... (by keyword?)
	* Whenever <action> happens		-> ... (should be easy, reference directly)

]]