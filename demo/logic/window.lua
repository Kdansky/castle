return {
	zones = { 
		{ 
			key = "Hand",
			types = { "hand" },
			place = { 0, 0.8, 1, 1 }
		},
		{
			key = "Battlefield",
			types = { "board", "grid" },
			place = { 0.2, 0, 1, 0.8 },
			size = { 5, 3 },	-- board/grid needs an NxN size
		},
		{
			key = "Deck",
			types = { "deck", "hidden" },
		},
		{
			key = "Graveyard",
			types = { "deck", "hidden" }
		},
		{
			key = "Player Statistics",
			types = { "stats" },
			place = { 0, 0, 0.2, 0.7 },
		},
		{
			key = "Menu",
			types = { "board", "grid", "Menu" },
			place = { 0, 0.7, 0.2, 0.8 },
			size = { 3, 1 },
		}
	},
}
