return {
	lookAndFeel = {
		zones = {
			margins = 4,
		},
		background = {
			asset = "dark_bg2.jpg"
		},
		font = {
			name = "GermaniaOne-Regular.ttf",
			size = 18,
		},
		video = {
			width = 1824,
			height = 1026,
		},
		tooltips = {
			{
				key = "full",
				types = { "tooltip" },
				margins = 8,
				placeStats = { 0.1, 0.1, 0.4, 0.2 },
				placePic = { 0.1, 0.2, 0.4, 0.9 },
				placeText = { 0.4, 0.1, 0.8, 0.9 },
			},
			{
				key = "small",
				types = { "tooltip" },
				margins = 4,
				placeStats = { 0.1, 0.1, 0.4, 0.2 },
				placePic = { 0.1, 0.2, 0.4, 0.7 },
				placeText = { 0.1, 0.7, 0.4, 0.9 },
			}
		}
	}
}
