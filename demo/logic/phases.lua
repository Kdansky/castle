return {
	phases = { 
		{ 
			key = "Init",
			types = { "automatic" },
			actions = { "Setup The Field" },
		}, 
		{
			key = "Upkeep",
			types = {"automatic"},
			actions = {"Draw Card", "Refresh Mana", "Refresh Ready"}
		},
		{ 
			key = "Play",
			types = { "player_input", "manual_end" },
			actions = { 
				--"RClick To Create A Dragon", 
				--"Keyboard: A", 
				--"Keyboard: D",
				"Play Card",
				"Activate A Button",
			},
			successor = "Upkeep",
		}
	},
	squares = {
		{
			key = "Place",
			color = { 0.1, 0.2, 0.1 },
			types = { "inhabitable", },
			text = "This is a place where tokens can live",
			asset = "hills.jpg",
			stats = { Health = 3 },
		},
		{
			key = "End Turn",
			types = { "Button" },
			asset = "harpy.jpg",
			text = "Ends your turn",
			command = { "end_phase" }
		},
	},
	stats = {
		{
			key = "Ready",
			text = "Whether a square or token can still be activated this turn",
			min = 0,
			max = 1,
			types = { "integer", "ignore_overflow" },
			color = { 0, 0.5, 0.9 },
		}
	},
}