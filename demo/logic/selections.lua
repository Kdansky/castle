return {
	cards = {
		key = "Double Strike",
		asset = "missing",
		text = "This spell lets you choose two things to hit for two damage each",
		style = "legendary",
		types = {},
		query = {},
		command = {
			"select_one_action", -- any time select is used, we have a new context.
				{},
				{},
			},
		actions = { "Spell" },
		stats = { Mana = 4, Attack = 2 },	
	},
}

-- What do I want to write? Examples:

--   "Choose one: Either give +3 HP to a minion, or deal 3 damage to a minion."
-- select one out of two arbitrary things, then use the resulting q/c on the board
-- Essentially: "select between two actions" / "choose" is different from multi targetting? 
-- uses a temporary space with options to click on?
-- choose is very likely to result in a followup selector

-- multi selection / complex target choices.

--   "Select three minions, deal 1 damage to each of them."
-- Just run the same query three times, but make it impossible to select the same thing? Fuck that's hard.
-- Then use the same command on all targets

--  "Select two minions, have them each deal their attack as damage to the other."
-- even worse: multiple targets, but complicated commands that are very context-heavy

-- Maybe I need a way to dynamically add eval functions? Like being able to declare "deal damage" without having to write the 
-- whole decrease_stat, hp, number thing every time?


-- How about forcing a follow-up action? E.g. go to a phase that has only a single action defined, so it must be completed.
-- Temporary phases? Possibly have a stack of phases if needed?
-- Could make an action that waits for multiple selected things afterwards.

