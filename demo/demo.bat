:: Need to go to the directory of the game for requires to work correctly
cd ..

:: clear screen for easier console output
cls

:: start the love engine, pass it the current folder, plus the name of the content folder
..\love-11.3-win64\love.exe . "demo"